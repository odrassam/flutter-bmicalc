import 'package:flutter/material.dart';
import '../constant.dart';

class IconContent extends StatelessWidget {
  IconContent({@required this.icon, this.iconTitle});
  final IconData icon;
  final String iconTitle;
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Icon(
          icon,
          size: 80.0,
        ),
        Text(iconTitle, style: labelTextStyle)
      ],
    );
  }
}